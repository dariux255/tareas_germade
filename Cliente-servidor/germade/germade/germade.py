
print("////////////////////Servidor////////////////////")
import socket

ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #Puerto y servidor que escucha
ser.bind(("", 8050))

while True:

    #Aceptamos conexiones entrantes con el metodo listen. Por parámetro las conexiones simutáneas.
    ser.listen()

    #Instanciamos un objeto cli (socket cliente) para recibir datos
    cli, addr = ser.accept()


    while True:
        try:
    #Recibimos el mensaje, con el metodo recv recibimos datos. Por parametro la cantidad de bytes para recibir
            recibido = cli.recv(1024)

    #Si se reciben datos nos muestra la IP y el mensaje recibido
            print("Recibo conexion de la IP: " + str(addr[0]) + " Puerto: " + str(addr[1]))
            print(recibido)
    #Devolvemos el mensaje al cliente
    #cli.send(“mensaje recibido”)
            msg_toSend=("mensaje recibido")
            cli.send(msg_toSend.encode('ascii'))
        except:
            print("conexion con: " + str(addr[0]) + " Puerto: " + str(addr[1]) + " cerrada")
            break


#Cerramos la instancia del socket cliente y servidor
cli.close()